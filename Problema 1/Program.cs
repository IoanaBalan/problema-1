﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int nr = int.Parse(Console.ReadLine());

            int[] numere = new int[nr];

            for (int j = 0; j < nr; j++)
                numere[j] = int.Parse(Console.ReadLine());

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            int k = 0;
            int suma = 0;
            for (int i = 0; i < nr; i++)
                if ((numere[i] >= a && numere[i] <= b) || (numere[i] <= a && numere[i] >= b))
                {
                    suma += numere[i];
                    k++;
                }
            double media = (double)suma / k;
            Console.WriteLine("Media elementelor este " + media);

            Console.ReadKey();
        }
    }
}